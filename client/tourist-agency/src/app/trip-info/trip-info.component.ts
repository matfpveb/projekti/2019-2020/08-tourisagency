import { Component, OnInit, OnDestroy } from '@angular/core';
import { TourInfoService } from '../services/tour-info.service'
import { TourInfoGetService } from '../services/tour-info-get.service';
import { Observable, of } from 'rxjs';
import { ReservationsService } from '../services/reservations.service';

@Component({
  selector: 'app-trip-info',
  templateUrl: './trip-info.component.html',
  styleUrls: ['./trip-info.component.css']
})
export class TripInfoComponent implements OnInit, OnDestroy {
  public tripName: string;
  public offerInput;

  constructor(private tsv: TourInfoService, private tig: TourInfoGetService, private res: ReservationsService) {
    this.tripName = tsv.getTripNameTIS();
    tig.url = "http://localhost:3000/api/offers/" + this.tripName;
    this.offerInput = tig.httpConnect();

    this.tig.getOffers().subscribe(offers => {
      for(let i in offers) {
        // console.log(offers[i]);
      }
    });
  }

  check(visits: string): boolean {
    if(visits.length == 0) {
        return true;
    }
    return false;
  }

  onClick(data) {
    this.res.addToReservation(data);
  }

  ngOnInit() {
  }

  ngOnDestroy(){
  }

}
