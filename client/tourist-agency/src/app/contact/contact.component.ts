import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  public contactForm : FormGroup;
  public submitedSuccessfuly : boolean = false;
  private readonly url : string = "http://localhost:3000/api/inboxes";

  constructor(private formBuilder: FormBuilder, private http : HttpClient) { 
    this.contactForm = this.formBuilder.group({
      name:   ["", [Validators.required]],
      email:  ["", [Validators.required, Validators.email]],
      message:["", [Validators.required, Validators.minLength(10)]],
      subject:["", [Validators.required]]
    });
  }

  ngOnInit() {
  }

  public get name() {
    return this.contactForm.get("name");
  }

  public get email() {
    return this.contactForm.get("email");
  }

  public get message() {
    return this.contactForm.get("message");
  }

  public get subject() {
    return this.contactForm.get("subject");
  }

  public submitForm(data): void {
    console.log(data);
    this.http.post(this.url, data).subscribe(
      (val) => { console.log("POST call successful ", val)},
      (response) => { console.log("POST call error ", response)},
      () => {console.log("POST over")}
    );

    this.submitedSuccessfuly = true;
    setTimeout( ()=>{this.submitedSuccessfuly = false;}, 3500);
    this.contactForm.reset();
    return;
  }
}
