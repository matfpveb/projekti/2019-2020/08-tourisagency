import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlogComponent } from './blog/blog.component';
import { ContactComponent} from './contact/contact.component';
import { HomeComponent} from './home/home.component';
import { ToursListComponent} from './tours-list/tours-list.component'
import { TripInfoComponent } from './trip-info/trip-info.component';
import { CheckoutComponent } from './checkout/checkout.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'blog', component: BlogComponent},
  {path: 'tours/:tourID', component: ToursListComponent},
  {path: 'tour-info', component: TripInfoComponent},
  {path: 'checkout', component: CheckoutComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
