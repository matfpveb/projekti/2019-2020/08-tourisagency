import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { TourInfoService } from '../services/tour-info.service';

@Component({
  selector: 'app-tours-list',
  templateUrl: './tours-list.component.html',
  styleUrls: ['./tours-list.component.css']
})
export class ToursListComponent implements OnInit, OnDestroy {
  private paramMapSub: Subscription = null;
  public url: string = "";
  public tripType: string = "";
  public tripTypeL: string = "";
  public trips = new Set<string>();

  constructor(private route: ActivatedRoute, private http: HttpClient, private tripNameService: TourInfoService, private router: Router) {
    this.paramMapSub = route.paramMap.subscribe(params => {
      this.trips.clear();

      this.setTripType(params.get('tourID') as string);
      tripNameService.setTripNameTIS(params.get('tourID') as string);

      this.url = "http://localhost:3000/api/offers/" + this.tripType;
      this.http.get(this.url).toPromise().then(data => {
          for(let i in data) {
            this.addToTrips(data[i]['title'] as string);
          }
      });
    });
  }

  addToTrips(data: string) {
    this.trips.add(data);
  }

  public setTripType(newType : string) {
    if(newType != this.tripType) {
      this.tripType = newType;

      switch (this.tripType) {
        case "ec":  this.tripTypeL = "European cities"; break;
        case "r" :  this.tripTypeL = "Roundtrip"; break;
        case "c" :  this.tripTypeL = "Cruisers"; break;
        case "st":  this.tripTypeL = "Summertime"; break;
        case "wt":  this.tripTypeL = "Wintertime"; break;
        case "fd":  this.tripTypeL = "Far destinations"; break;
      }
    }
  } 

  public getTripType() : string {
    return this.tripType;
  }

  onClick(name :string) {
    this.tripNameService.setTripNameTIS(name);
    this.router.navigateByUrl('/tour-info')
  }
  

  ngOnInit() {
  }

  ngOnDestroy() {
    this.paramMapSub.unsubscribe();
  }
}
