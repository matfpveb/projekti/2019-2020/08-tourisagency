import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  public subscribeForm : FormGroup;
  public submitedSuccessfuly : boolean = false;
  private readonly url : string = "http://localhost:3000/api/newsletters";

  constructor(private formBuilder : FormBuilder, private http : HttpClient) { 
    this.subscribeForm = formBuilder.group({
      email : ["", [Validators.required, Validators.email]]
    });
  }

  ngOnInit() {
  }

  public submitForm(data) : void {
    console.log(data);
    this.http.post(this.url, data).subscribe(
      (val) => { console.log("POST call successful ", val)},
      (response) => { console.log("POST call error ", response)},
      () => {console.log("POST over")}
    );

    this.submitedSuccessfuly = true;
    setTimeout(() => {this.submitedSuccessfuly = false;}, 3500)
    this.subscribeForm.reset();
    return;
  }

}
