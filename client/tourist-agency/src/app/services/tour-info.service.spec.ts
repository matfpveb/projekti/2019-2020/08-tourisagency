import { TestBed } from '@angular/core/testing';

import { TourInfoService } from './tour-info.service';

describe('TourInfoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TourInfoService = TestBed.get(TourInfoService);
    expect(service).toBeTruthy();
  });
});
