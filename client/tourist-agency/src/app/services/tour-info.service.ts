import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TourInfoService {
  public tripName: string;

  constructor() { 

  }

  setTripNameTIS(newTN: string) {
    this.tripName = newTN;
  }

  getTripNameTIS() : string {
    return this.tripName;
  }
}
