import { Injectable } from '@angular/core';

export interface ReservationPOST {
  no: string;
  customerId: string;
  offerId: string;
  dateStart: string;
  dateEnd: string;
}

@Injectable({
  providedIn: 'root'
})
export class ReservationsService {
  public allReservations: any[] = [];
  public allReservationsPOST: any[] =[];
  private n: number = 1;

  constructor() { }

  addToReservation(newReservation) {
    newReservation['no'] = String(this.n);
    this.n = this.n + 1;
    this.allReservations.push(newReservation);
    
    const reservationPost: ReservationPOST = {  no: newReservation['no'], customerId: null, offerId: newReservation['_id'], dateStart: null, dateEnd: null};
    this.allReservationsPOST.push(reservationPost);                      
  }

  getAllReservations() {
    return this.allReservations;
  }

  getAllReservationsPOST() {
    return this.allReservationsPOST;
  }
}
