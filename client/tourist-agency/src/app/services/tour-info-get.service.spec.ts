import { TestBed } from '@angular/core/testing';

import { TourInfoGetService } from './tour-info-get.service';

describe('TourInfoGetService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TourInfoGetService = TestBed.get(TourInfoGetService);
    expect(service).toBeTruthy();
  });
});
