import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscription, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TourInfoGetService {
  public url: string;
  public data;
  public offers;

  constructor(private http : HttpClient) {
  }

  httpConnect() {
    this.offers = this.http.get(this.url);
    return this.offers;
  }

  getOffers() {
    return this.offers;
  }
}