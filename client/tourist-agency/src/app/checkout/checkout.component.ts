import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReservationsService } from '../services/reservations.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  public customerForm : FormGroup;
  public datesEntered : boolean = false;
  public readonly urlRes : string = 'http://localhost:3000/api/reservations';
  public readonly urlCus : string = 'http://localhost:3000/api/customers';

  public allReservations;
  public allReservationsPOST;
  public minDate = new Date();
  public days: number;

  constructor(private res: ReservationsService, private formBuilder: FormBuilder, private http: HttpClient) { 
    this.customerForm = this.formBuilder.group({
      name:   ["", [Validators.required]],
      phone:  ["", [Validators.required, Validators.minLength(5)]]
    });

    this.minDate.setDate(this.minDate.getDate() + 1);
    this.allReservations = res.getAllReservations();
    this.allReservationsPOST = res.getAllReservationsPOST();
  }

  onDateChange(event: Event, no: string) {
    const startDate = new Date((<HTMLInputElement>event.target).value);

    for(let i in this.allReservationsPOST) {
      if(this.allReservationsPOST[i]['no'] == no) {
        const s = startDate.getFullYear() + '-' + startDate.getMonth() + '-' + startDate.getDay();
        this.allReservationsPOST[i]['dateStart'] = startDate.getTime();

        for(let j in this.allReservations) {
          if(this.allReservations[j]['_id'] == this.allReservationsPOST[i]['offerId']) {
            this.days = this.allReservations[j]['numberOfDays'];
          }
        }
        const endDate = new Date(startDate.getTime() + this.days*86400000);
        const e = endDate.getFullYear() + '-' + endDate.getMonth() + '-' + endDate.getDay();
        this.allReservationsPOST[i]['dateEnd'] = endDate.getTime();
      }
    }
    
    this.datesEntered = true;
    for(let i in this.allReservationsPOST) {
      if(this.allReservationsPOST[i]['dateStart'] == null) {
        this.datesEntered = false;
      }
    }
  }

  public datesEnteredF() {
    return !this.datesEntered;
  }

  public async submitForm(data) {
    if(this.datesEnteredF()) {
      return;
    }
    const val = await this.http.post(this.urlCus, data).toPromise();
    for(let i in this.allReservationsPOST) {
      this.allReservationsPOST[i]['customerId'] = val['_id'];
      this.allReservationsPOST[i]['no'] = undefined;
      const valr = await this.http.post(this.urlRes, this.allReservationsPOST[i]).toPromise();
    }
  }

  totalPrice() {
    var total: number = 0;
    for(let i in this.allReservations)
      total += this.allReservations[i]['dailyPrice'] * this.allReservations[i]['numberOfDays'];
    return total;
  }

  ngOnInit() {
  }

}
