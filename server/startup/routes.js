const express = require("express");
const locations = require("../routes/locations");
const customers = require("../routes/customers");
const offers = require("../routes/offers");
const reservations = require("../routes/reservations");
const users = require("../routes/users");
const nl = require("../routes/newsletters");
const inbox = require("../routes/inboxes");
const hotel = require("../routes/hotels");
const cruiser = require("../routes/cruisers");
const auth = require("../routes/auth");
const error = require("../middleware/error");

module.exports = function (app) {
  app.use(express.json());
  app.use("/api/locations", locations);
  app.use("/api/customers", customers);
  app.use("/api/offers", offers);
  app.use("/api/reservations", reservations);
  app.use("/api/users", users);
  app.use("/api/newsletters", nl);
  app.use("/api/inboxes", inbox);
  app.use("/api/hotels", hotel);
  app.use("/api/cruisers", cruiser);
  app.use("/api/auth", auth);
  app.use(error);
};
