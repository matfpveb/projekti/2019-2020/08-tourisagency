## NOTE
All collections also have _id and __v
  
### newsletter
email (String, required), date (Date), unsubscribed(Boolean)
  
GET: localhost:3000/api/newsletters  
POST: localhost:3000/api/newsletters  
PUT: localhost:3000/api/newsletters/:id  
DELETE: localhost:3000/api/newsletters/:id  
GET: localhost:3000/api/newsletters/:id  

  
### inbox
name (String, required), email (String, required), subject (String, required), message (String, required)
  
GET: localhost:3000/api/inboxes  
POST: localhost:3000/api/inboxes  
PUT: localhost:3000/api/inboxes/:id  
DELETE: localhost:3000/api/inboxes/:id
GET: localhost:3000/api/inboxes/:id


### location
name (String, required), region (String, required)

GET: localhost:3000/api/locations  
POST: localhost:3000/api/locations  
PUT: localhost:3000/api/locations/:id  
DELETE: localhost:3000/api/locations/:id  
GET: localhost:3000/api/locations/:name  
GET: localhost:3000/api/locations/:region  

### hotel
name (String, required), location (locationSchema, required), stars (Number)
  
GET: localhost:3000/api/hotels  
POST: localhost:3000/api/hotels  
GET: localhost:3000/api/hotels/:name  
GET: localhost:3000/api/hotels/location/:name <-- this returns an array of objects (not sure if it works)  

### cruiser
name (String, required), roomType (String, required)

GET: localhost:3000/api/cruisers  
POST: localhost:3000/api/cruisers  
GET: localhost:3000/api/cruisers/:name  

### offers 
title (String, required), tripType (String, required[ec|r|st|wt|fd for hotel and c for cruiser]), hotel (hotelSchema, required if tripType != c), cruiser (cruiserSchema, required if tripType == c), available (Number, required), dailyPrice (Number, required), numberOfDays (Number, required), includes (String), visits (array of Strings)

GET: localhost:3000/api/offers  
POST: localhost:3000/api/offers  
PUT: localhost:3000/api/offers/:id  
DELETE: localhost:3000/api/offers/:id  
GET: localhost:3000/api/offers/:title  
GET: localhost:3000/api/offers/:tripType  
