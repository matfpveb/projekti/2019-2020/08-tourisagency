const Joi = require('joi');
const mongoose = require('mongoose');

const inboxSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  email: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 255,
    unique: true
  },
  subject: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  message: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 1024
  }
});

const Inbox = mongoose.model('Inbox', inboxSchema);

function validateInbox(inbox) {
  const schema = {
    name: Joi.string().min(5).max(50).required(),
    email: Joi.string().min(5).max(255).required().email(),
    subject: Joi.string().min(5).max(50).required(),
    message: Joi.string().min(5).max(255).required()
  };

  return Joi.validate(inbox, schema);
}

exports.Inbox = Inbox;
exports.validate = validateInbox;
