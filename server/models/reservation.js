const Joi = require("joi");
const mongoose = require("mongoose");
const moment = require("moment");

const reservationSchema = new mongoose.Schema({
  customer: {
    type: new mongoose.Schema({
      name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50,
      },
      phone: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 50,
      },
    }),
    required: true,
  },
  offer: {
    type: new mongoose.Schema({
      title: {
        type: String,
        required: true,
        trim: true,
        minlength: 5,
        maxlength: 255,
      },
      dailyPrice: {
        type: Number,
        required: true,
        min: 0,
        max: 255,
      },
    }),
    required: true,
  },
  dateStart: {
    type: Date,
    required: true,
  },
  dateEnd: {
    type: Date,
    required: true,
  },
  totalFee: {
    type: Number,
    min: 0,
  },
});

reservationSchema.statics.lookup = function (customerId, offerId) {
  return this.findOne({
    "customer._id": customerId,
    "offer._id": offerId,
  });
};

reservationSchema.methods.calculate = function () {
  const start = moment(this.dateStart);
  const end = moment(this.dateEnd);

  const totalDays = Math.abs(start.diff(end, "days"));
  this.totalFee = totalDays * this.offer.dailyPrice;
};

const Reservation = mongoose.model("Reservation", reservationSchema);

function validateReservation(reservation) {
  const schema = {
    customerId: Joi.objectId().required(),
    offerId: Joi.objectId().required(),
    dateStart: Joi.date().required(),
    dateEnd: Joi.date().required(),
  };

  return Joi.validate(reservation, schema);
}

exports.Reservation = Reservation;
exports.validate = validateReservation;
