const Joi = require("joi");
const mongoose = require("mongoose");
const { locationSchema } = require("./location");

const hotelSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50,
  },
  location: {
    type: locationSchema,
    required: true
  },
  stars: {
    type: Number,
    min: 1,
    max: 5
  }
});

const Hotel = mongoose.model("Hotel", hotelSchema);

function validateHotel(hotel) {
  const schema = {
    name: Joi.string().min(5).max(50).required(),
    locationId: Joi.objectId().required(),
    stars: Joi.number().min(1).max(5)
  };

  return Joi.validate(hotel, schema);
}

exports.hotelSchema = hotelSchema;
exports.Hotel = Hotel;
exports.validate = validateHotel;
