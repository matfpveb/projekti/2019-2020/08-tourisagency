const Joi = require("joi");
const mongoose = require("mongoose");
const { hotelSchema } = require("./hotel");

const Offer = mongoose.model(
  "Offers",
  new mongoose.Schema({
    title: {
      type: String,
      required: true,
      trim: true,
      minlength: 5,
      maxlength: 255,
    },
    tripType: { // h for hotel, c for cruise tour
      type: String,
      required: true,
      minlength: 1,
      maxlength: 2,
    },
    hotel: {
      type: new mongoose.Schema({
        name: {
          type: String,
          required: true,
          minlength: 5,
          maxlength: 50,
        },
        stars: {
          type: Number,
          min: 1,
          max: 5
        }
      }),
      required: function() {
        return this.tripType === "ec" || this.tripType === "r" || this.tripType === "st" || this.tripType === "wt" || this.tripType === "fd"; // hotel is required only if tripType includes hotel
      },
    },
    cruiser: {
      type: new mongoose.Schema({
        name: {
          type: String,
          required: true,
          minlength: 5,
          maxlength: 50,
        },
        roomType: {
          type: String,
          required: true,
          minlength: 5,
          maxlength: 50,
        }
      }),
      required: function() {
        return this.tripType === "c"; // cruiser is required only if tripType includes cruiser
      },
    },
    available: {
      type: Number,
      required: true,
      min: 0,
      max: 255,
    },
    dailyPrice: {
      type: Number,
      required: true,
      min: 0,
      max: 1000,
    },
    numberOfDays: {
      type: Number,
      required: true,
      min: 1,
      max: 30
    },
    includes: { // breakfast, lunch, all inclusive
      type: String,
      min: 0,
      max: 255,
    },
    visits: { // visit to Skadradlija, Hram Svetog Save etc
      type: [String],
      default: undefined
    }
  })
);

function validateOffer(offer) {
  const schema = {
    title: Joi.string().min(5).max(50).required(),
    tripType: Joi.string().min(1).max(2).required(),
    hotelId: Joi.objectId(),
    cruiserId: Joi.objectId(),
    available: Joi.number().min(0).max(255).required(),
    dailyPrice: Joi.number().min(0).max(1000).required(),
    numberOfDays: Joi.number().min(1).max(30).required(),
    includes: Joi.string().min(0).max(255),
    visits: Joi.array()
  };

  return Joi.validate(offer, schema);
}

exports.Offer = Offer;
exports.validate = validateOffer;
