const Joi = require('joi');
const mongoose = require('mongoose');

const newsletterSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 255,
    unique: true
  },
  date: {
    type: Date
  },
  unsubscribed: {
    type: Boolean
  }
});

newsletterSchema.methods.generate = function () {
  this.date = new Date();
  this.unsubscribed = false;
};


const NewsLetter = mongoose.model('NewsLetter', newsletterSchema);

function validateNewsletter(nl) {
  const schema = {
    email: Joi.string().min(5).max(255).required().email()
  };

  return Joi.validate(nl, schema);
}

exports.NewsLetter = NewsLetter;
exports.validate = validateNewsletter;
