const Joi = require("joi");
const mongoose = require("mongoose");

const cruiserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50,
  },
  roomType: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50,
  }
});

const Cruiser = mongoose.model("Cruiser", cruiserSchema);

function validateCruiser(cruiser) {
  const schema = {
    name: Joi.string().min(5).max(50).required(),
    roomType: Joi.string().min(5).max(50).required()
  };

  return Joi.validate(cruiser, schema);
}

exports.cruiserSchema = cruiserSchema;
exports.Cruiser = Cruiser;
exports.validate = validateCruiser;
