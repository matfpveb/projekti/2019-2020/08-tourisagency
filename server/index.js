const winston = require('winston');
const express = require('express');
const path = require('path');
const app = express();

require('./startup/logging')();
require('./startup/routes')(app);
require('./startup/db')();
require('./startup/config')();
require('./startup/validation')();

const port = process.env.PORT || 3000;
const server = app.listen(port, () => winston.info(`Listening on port ${port}...`));

// app.get('/', (req, res) => {
//   res.sendFile(path.join(__dirname, "../index.html"));
// });

//app.use(express.static(path.join(__dirname, "../")))

module.exports = server;
