const validateObjectId = require("../middleware/validateObjectId");
const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const { NewsLetter, validate } = require("../models/newsletter");
const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();

router.get("/", async (req, res) => {
  const nl = await NewsLetter.find().sort("email");
  res.send(nl);
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let nl = new NewsLetter({ email: req.body.email });

  nl.generate();

  nl = await nl.save();

  res.send(nl);
});

router.put("/:id",  validateObjectId, async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const nl = await NewsLetter.findByIdAndUpdate(
    req.params.id,
    { email: req.body.email },
    {
      new: true,
    }
  );

  if (!nl)
    return res
      .status(404)
      .send("The newsletters email with the given ID was not found.");

  res.send(nl);
});

router.delete("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const nl = await NewsLetter.findByIdAndRemove(req.params.id);

  if (!nl)
    return res
      .status(404)
      .send("The newsletters email with the given ID was not found.");

  res.send(nl);
});

router.get("/:id", validateObjectId, async (req, res) => {
  const nl = await NewsLetter.findById(req.params.id);

  if (!nl)
    return res
      .status(404)
      .send("The newsletters email with the given ID was not found.");

  res.send(nl);
});

module.exports = router;
