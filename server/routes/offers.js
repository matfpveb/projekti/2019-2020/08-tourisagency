const { Offer, validate } = require("../models/offer");
const { Cruiser } = require("../models/cruiser");
const { Hotel } = require("../models/hotel");
const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();

router.get("/", async (req, res) => {
  const offers = await Offer.find().sort("title");
  res.send(offers);
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const hasHotel = req.body.tripType === "ec" || req.body.tripType === "r" || req.body.tripType === "st"
                    || req.body.tripType === "wt" || req.body.tripType === "fd";
  const hasCruiser = req.body.tripType === "c";


  const cruiser = null;
  const hotel = null;

  if(hasHotel) {
     this.hotel = await Hotel.findById(req.body.hotelId);
    if (!this.hotel) return res.status(400).send("Invalid hotel.");
  }

  if(hasCruiser) {
     this.cruiser = await Cruiser.findById(req.body.cruiserId);
    if (!this.cruiser) return res.status(400).send("Invalid cruiser.");
  }

  const offer = new Offer({
    title: req.body.title,
    tripType: req.body.tripType,
    hotel: hasHotel ? {
      _id: this.hotel._id,
      name: this.hotel.name,
      stars: this.hotel.stars,
    } : null,
    cruiser: hasCruiser ? {
      _id: this.cruiser._id,
      name: this.cruiser.name,
      roomType: this.cruiser.roomType,
    } : null,
    available: req.body.available,
    dailyPrice: req.body.dailyPrice,
    numberOfDays: req.body.numberOfDays,
    includes: req.body.includes,
    visits: req.body.visits,
  });
  await offer.save();

  res.send(offer);
});

router.put("/:id", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const hasHotel = req.body.tripType === "ec" || req.body.tripType === "r" || req.body.tripType === "st"
                      || req.body.tripType === "wt" || req.body.tripType === "fd";
  const hasCruiser = req.body.tripType === "c";

  const hotel = null;
  const cruiser = null;

  if(hasHotel) {
     this.hotel = await Hotel.findById(req.body.hotelId);
    if (!this.hotel) return res.status(400).send("Invalid hotel.");
  }

  if(hasCruiser) {
     this.cruiser = await Cruiser.findById(req.body.cruiserId);
    if (!this.cruiser) return res.status(400).send("Invalid cruiser.");
  }

  const offer = await Offer.findByIdAndUpdate(
    req.params.id,
    {
      title: req.body.title,
      tripType: req.body.tripType,
      hotel: hasHotel ? {
        _id: this.hotel._id,
        name: this.hotel.name,
        stars: this.hotel.stars,
      } : null,
      cruiser: hasCruiser ? {
        _id: this.cruiser._id,
        name: this.cruiser.name,
        roomType: this.cruiser.roomType,
      } : null,
      available: req.body.available,
      dailyPrice: req.body.dailyPrice,
      numberOfDays: req.body.numberOfDays,
      includes: req.body.includes,
      visits: req.body.visits,
    },
    { new: true }
  );

  if (!offer)
    return res.status(404).send("The offer with the given ID was not found.");

  res.send(offer);
});

router.delete("/:id", async (req, res) => {
  const offer = await Offer.findByIdAndRemove(req.params.id);

  if (!offer)
    return res.status(404).send("The offer with the given ID was not found.");

  res.send(offer);
});

// router.get("/:id", async (req, res) => {
//   const offer = await Offer.findById(req.params.id);
//
//   if (!offer)
//     return res.status(404).send("The offer with the given ID was not found.");
//
//   res.send(offer);
// });

router.get("/:param", async (req, res) => {
  const offer = null;

  if(mongoose.Types.ObjectId.isValid(req.params.param)) {
    this.offer = await Offer.findById(req.params.param);
    if (!this.offers)
      return res
        .status(404)
        .send("The offer with the given ID was not found.");
  }
  else {
    this.offer = await Offer.find().or([{"title": req.params.param}, {"tripType": req.params.param}]);
    if (!this.offer)
      return res
        .status(404)
        .send("Nothing found.");
  }

  res.send(this.offer);
});

module.exports = router;
