const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const { Cruiser, validate } = require("../models/cruiser");
const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();

router.get("/", async (req, res) => {
  const cruiser = await Cruiser.find().sort("name");
  res.send(cruiser);
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const cruiser = new Cruiser({
    name: req.body.name,
    roomType: req.body.roomType
  });
  await cruiser.save();

  res.send(cruiser);
});

router.get("/:param", async (req, res) => {
  const cruiser = null;

  if(mongoose.Types.ObjectId.isValid(req.params.param)) {
    this.cruiser = await Cruiser.findById(req.params.param);
    if (!this.cruiser)
      return res
        .status(404)
        .send("The cruiser with the given ID was not found.");
  }
  else {
    this.cruiser = await Cruiser.find({"name": req.params.param});
    if (!this.cruiser)
      return res
        .status(404)
        .send("Nothing found.");
  }

  res.send(this.cruiser);
});

//TODO put and delete methods

// TODO get methods for id or name etc


module.exports = router;
