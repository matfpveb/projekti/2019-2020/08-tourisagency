const validateObjectId = require("../middleware/validateObjectId");
const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const { Hotel, validate } = require("../models/hotel");
const { Location } = require("../models/location");
const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();

router.get("/", async (req, res) => {
  const hotels = await Hotel.find().sort("name");
  res.send(hotels);
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const location = await Location.findById(req.body.locationId);
  if (!location) return res.status(400).send("Invalid location.");

  const hotel = new Hotel({
    name: req.body.name,
    location: {
      _id: location._id,
      name: location.name,
      region: location.region
    },
    stars: req.body.stars
  });
  await hotel.save();

  res.send(hotel);
});

router.get("/:param", async (req, res) => {
  const hotel = null;

  if(mongoose.Types.ObjectId.isValid(req.params.param)) {
    this.hotel = await Hotel.findById(req.params.param);
    if (!this.hotel)
      return res
        .status(404)
        .send("The hotel with the given ID was not found.");
  }
  else {
    this.hotel = await Hotel.find({"name": req.params.param});
    if (!this.hotel)
      return res
        .status(404)
        .send("Nothing found.");
  }

  res.send(this.hotel);
});

router.get("/location/:name", async (req, res) => {
  const location = await Location.find().or([{"name": req.params.name}, {"region": req.params.name}]);

  if (!location)
      return res
        .status(404)
        .send("The location with the given name was not found.");

  var idLocation;
  const hotels = [];
  for (var i = 0; i < location.length; i++) {
    idLocation = location[i]["_id"];
    const hotel = await Hotel.find({"location._id": new mongoose.Types.ObjectId(idLocation)});

    hotels.push(hotel);
  }
  // location[i]["_id"];

  // var id = new mongoose.Types.ObjectId(idLocation);

  // const hotel = await Hotel.find({"location._id": id});

  if (!hotels)
    return res
      .status(404)
      .send("Nothing found.");

  res.send(hotels);
});


//TODO put and delete methods

// TODO get methods for id or name etc


module.exports = router;
