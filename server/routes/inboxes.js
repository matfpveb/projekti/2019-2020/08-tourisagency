const validateObjectId = require("../middleware/validateObjectId");
const auth = require('../middleware/auth');
const admin = require("../middleware/admin");
const config = require('config');
const {Inbox, validate} = require('../models/inbox');
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();


router.get("/", async (req, res) => {
  const inbox = await Inbox.find().sort("email");
  res.send(inbox);
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let inbox = new Inbox({
    name: req.body.name,
    email: req.body.email,
    subject: req.body.subject,
    message: req.body.message
  });

  inbox = await inbox.save();

  res.send(inbox);
});

router.put("/:id", validateObjectId, async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const inbox = await Inbox.findByIdAndUpdate(
    req.params.id,
    {
      name: req.body.name,
      email: req.body.email,
      subject: req.body.subject,
      message: req.body.message
    },
    {
      new: true,
    }
  );

  if (!inbox)
    return res
      .status(404)
      .send("The message from inbox with the given ID was not found.");

  res.send(inbox);
});

router.delete("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const inbox = await Inbox.findByIdAndRemove(req.params.id);

  if (!inbox)
    return res
      .status(404)
      .send("The message from inbox with the given ID was not found.");

  res.send(inbox);
});

router.get("/:id", validateObjectId, async (req, res) => {
  const inbox = await inbox.findById(req.params.id);

  if (!inbox)
    return res
      .status(404)
      .send("The message from inbox with the given ID was not found.");

  res.send(inbox);
});


module.exports = router;
