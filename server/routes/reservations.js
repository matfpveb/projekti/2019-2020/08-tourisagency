const { Reservation, validate } = require("../models/reservation");
const { Offer } = require("../models/offer");
const { Customer } = require("../models/customer");
const mongoose = require("mongoose");
const Fawn = require("fawn");
const express = require("express");
const router = express.Router();

Fawn.init(mongoose);

router.get("/", async (req, res) => {
  const reservations = await Reservation.find().sort("-dateStart");
  res.send(reservations);
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const customer = await Customer.findById(req.body.customerId);
  if (!customer) return res.status(400).send("Invalid customer.");

  const offer = await Offer.findById(req.body.offerId);
  if (!offer) return res.status(400).send("Invalid offer.");

  if (offer.available === 0) return res.status(400).send("Everything is taken");

  let reservation = new Reservation({
    customer: {
      _id: customer._id,
      name: customer.name,
      phone: customer.phone,
    },
    offer: {
      _id: offer._id,
      title: offer.title,
      dailyPrice: offer.dailyPrice,
    },
    dateStart: req.body.dateStart,
    dateEnd: req.body.dateEnd,
    totalFee: 0,
  });

  reservation.calculate();
  //await reservation.save();

  try {
    new Fawn.Task()
      .save("reservations", reservation)
      .update(
        "offers",
        { _id: offer._id },
        {
          $inc: { available: -1 },
        }
      )
      .run();

    res.send(reservation);
  } catch (ex) {
    res.status(500).send("Something failed.");
  }
});

router.get("/:id", async (req, res) => {
  const reservation = await Reservation.findById(req.params.id);

  if (!reservation)
    return res
      .status(404)
      .send("The reservation with the given ID was not found.");

  res.send(reservation);
});

module.exports = router;
