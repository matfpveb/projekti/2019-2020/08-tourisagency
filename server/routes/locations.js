const validateObjectId = require("../middleware/validateObjectId");
const auth = require("../middleware/auth");
const admin = require("../middleware/admin");
const { Location, validate } = require("../models/location");
const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();

router.get("/", async (req, res) => {
  const locations = await Location.find().sort("name");
  res.send(locations);
});

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let location = new Location({
    name: req.body.name,
    region: req.body.region
   });
  location = await location.save();

  res.send(location);
});

router.put("/:id", [auth, validateObjectId], async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const location = await Location.findByIdAndUpdate(
    req.params.id,
    {
      name: req.body.name,
      region: req.body.region
     },
    {
      new: true,
    }
  );

  if (!location)
    return res
      .status(404)
      .send("The location with the given ID was not found.");

  res.send(location);
});

router.delete("/:id", [auth, admin, validateObjectId], async (req, res) => {
  const location = await Location.findByIdAndRemove(req.params.id);

  if (!location)
    return res
      .status(404)
      .send("The location with the given ID was not found.");

  res.send(location);
});

// router.get("/:id", validateObjectId, async (req, res) => {
//   const location = await Location.findById(req.params.id);
//
//   if (!location)
//     return res
//       .status(404)
//       .send("The location with the given ID was not found.");
//
//   res.send(location);
// });

router.get("/:param", async (req, res) => {
  const location = null;

  if(mongoose.Types.ObjectId.isValid(req.params.param)) {
    this.location = await Location.findById(req.params.param);
    if (!this.location)
      return res
        .status(404)
        .send("The location with the given ID was not found.");
  }
  else {
    this.location = await Location.find().or([{"name": req.params.param}, {"region": req.params.param}]);
    if (!this.location)
      return res
        .status(404)
        .send("Nothing found.");
  }

  res.send(this.location);
});

module.exports = router;
