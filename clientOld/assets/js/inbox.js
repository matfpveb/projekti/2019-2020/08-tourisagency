function sendMessage() {
    var name = document.forms["inboxform"]["name"].value;
    var emailadress = document.forms["inboxform"]["email"].value;
    var subject = document.forms["inboxform"]["subject"].value;
    var message = document.forms["inboxform"]["message"].value;
    var xhr = new XMLHttpRequest();
    var url = "http://localhost:3000/api/inboxes/";
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            console.log(xhr.responseText);
            windows.alert("You have sent message successfully. Thanks !");
        }
    };
    
    var data = JSON.stringify({"name": String(name), "email": String(emailadress),"subject": String(subject),"message": String(message)});
    xhr.send(data);
}

