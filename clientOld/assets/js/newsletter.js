function subscribeToNewsletter() {
    console.log("OK");
    
    
    var emailadress = document.forms["newsletterform"]["email"].value;
    var xhr = new XMLHttpRequest();
    var url = "http://localhost:3000/api/newsletters/";
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            console.log(xhr.responseText);
            windows.alert("You have subscribed successfuly. Thanks !");
        }
    };
    
    var data = JSON.stringify({"email": String(emailadress)});
    xhr.send(data);
}
